package com.cerezaconsulting.cocosapp.presentation.main.buscador;

import com.cerezaconsulting.cocosapp.core.BasePresenter;
import com.cerezaconsulting.cocosapp.core.BaseView;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.presentation.main.listrestaurante.ListRestaurantContract;

import java.util.ArrayList;

/**
 * Created by kath on 11/01/18.
 */

public class BuscadorContract {
    interface View extends BaseView<Presenter> {

        void getListRestaurante(ArrayList<RestEntinty> list);

        void clickItemRestaurante(RestEntinty restEntinty);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {


        void loadOrdersFromPage(int page, String search);

        void loadfromNextPage(String search);

        void startLoad(String search);

        void getRestaurante(int page, String search);

    }
}
