package com.cerezaconsulting.cocosapp.presentation.main.buscador;

import android.content.Context;

import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.data.entities.trackholder.TrackEntityHolder;
import com.cerezaconsulting.cocosapp.data.local.SessionManager;
import com.cerezaconsulting.cocosapp.data.remote.ServiceFactory;
import com.cerezaconsulting.cocosapp.data.remote.request.ListRequest;
import com.cerezaconsulting.cocosapp.presentation.main.listrestaurante.ListRestaurantContract;
import com.cerezaconsulting.cocosapp.presentation.main.listrestaurante.RestItem;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 28/06/17.
 */

public class BuscadorPresenter implements BuscadorContract.Presenter, RestItem {

    private BuscadorContract.View mView;
    private Context context;
    private SessionManager mSessionManager;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public BuscadorPresenter(BuscadorContract.View mView, Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "view cannot be null!");
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(this.context);
    }


    @Override
    public void start() {

    }


    @Override
    public void clickItem(RestEntinty restEntinty) {
        mView.clickItemRestaurante(restEntinty);
    }

    @Override
    public void deleteItem(RestEntinty restEntinty, int position) {

    }

    @Override
    public void loadOrdersFromPage(int page, String search) {
        getRestaurante(page, search);

    }

    @Override
    public void loadfromNextPage(String search) {

        if (currentPage > 0)
            getRestaurante(currentPage, search);

    }

    @Override
    public void startLoad(String search) {
        if (!firstLoad) {
            firstLoad = true;
            loadOrdersFromPage(1, search);
        }
    }

    @Override
    public void getRestaurante(final int page, String search) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<TrackEntityHolder<RestEntinty>> orders = listRequest.getRestaurantes("Token "+mSessionManager.getUserToken() ,page, search);
        orders.enqueue(new Callback<TrackEntityHolder<RestEntinty>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<RestEntinty>> call, Response<TrackEntityHolder<RestEntinty>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.getListRestaurante(response.body().getResults());

                    if (response.body().getNext() != null) {
                        currentPage = page +1;
                    } else {
                        currentPage = -1;
                    }

                } else {
                    mView.showErrorMessage("Error al obtener las órdenes");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<RestEntinty>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
