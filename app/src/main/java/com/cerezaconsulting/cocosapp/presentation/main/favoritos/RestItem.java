package com.cerezaconsulting.cocosapp.presentation.main.favoritos;


import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;

/**
 * Created by katherine on 24/04/17.
 */

public interface RestItem {

    void clickItem(RestEntinty restEntinty);

    void deleteItem(RestEntinty restEntinty, int position);
}
