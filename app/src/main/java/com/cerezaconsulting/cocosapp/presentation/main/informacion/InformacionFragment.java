package com.cerezaconsulting.cocosapp.presentation.main.informacion;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cerezaconsulting.cocosapp.R;
import com.cerezaconsulting.cocosapp.core.BaseFragment;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.data.entities.ServEntity;
import com.cerezaconsulting.cocosapp.data.entities.SubCatEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 28/06/17.
 */

public class InformacionFragment extends BaseFragment{


    @BindView(R.id.tv_servicio)
    TextView tvServicio;
    @BindView(R.id.tv_direccion)
    TextView tvDireccion;
    @BindView(R.id.tv_horario)
    TextView tvHorario;
    @BindView(R.id.tv_cell)
    TextView tvCell;
    @BindView(R.id.tv_carta)
    TextView tvCarta;
    Unbinder unbinder;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.tv_llevame)
    TextView tvLlevame;

    Map<String, Integer> perms = new HashMap<>();

    private SubCatEntity subCatEntity;
    private String daySelected;
    private ServicesAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private RestEntinty restEntinty;

    public InformacionFragment() {
        // Requires empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public static InformacionFragment newInstance() {
        //PromoFragment fragment = new PromoFragment();
        //fragment.setArguments(bundle);
        return new InformacionFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        restEntinty = (RestEntinty) bundle.getSerializable("restEntity");
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_information, container, false);
        unbinder = ButterKnife.bind(this, root);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvList.setLayoutManager(mLayoutManager);
        mAdapter = new ServicesAdapter(new ArrayList<ServEntity>(), getContext());
        rvList.setAdapter(mAdapter);

        if (restEntinty.getService() != null) {
            mAdapter.setItems(restEntinty.getService());
        }

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (restEntinty.getMobile() != null) {
            tvCell.setText(restEntinty.getMobile());
        }

        if (restEntinty.getSchedule() != null) {
            tvHorario.setText(restEntinty.getSchedule().get(0).getName());
        }

        if (restEntinty.getAddress() != null) {
            tvDireccion.setText(restEntinty.getAddress());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.tv_llevame, R.id.tv_carta})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_llevame:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + restEntinty.getLatitude() + "," + restEntinty.getLongitude()));
                getContext().startActivity(i);

                break;

            case R.id.tv_carta:
                Bundle bundle = new Bundle();
                bundle.putString("url", restEntinty.getFood_letter());
                nextActivity(getActivity(), bundle, VisorActivity.class, false);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
