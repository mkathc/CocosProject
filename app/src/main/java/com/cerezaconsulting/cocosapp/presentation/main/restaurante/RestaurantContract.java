package com.cerezaconsulting.cocosapp.presentation.main.favoritos;

import com.cerezaconsulting.cocosapp.core.BasePresenter;
import com.cerezaconsulting.cocosapp.core.BaseView;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.data.entities.RestaurantResponse;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface FavoritosContract {
    interface View extends BaseView<Presenter> {

        void getListRestaurante(ArrayList<RestaurantResponse> list);

        void clickItemRestaurante(RestaurantResponse restaurantResponse);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {


        void loadOrdersFromPage(int page );

        void loadfromNextPage();

        void startLoad();

        void getRestaurante(int page);

    }
}
