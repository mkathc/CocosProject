package com.cerezaconsulting.cocosapp.presentation.main.restaurante;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cerezaconsulting.cocosapp.R;
import com.cerezaconsulting.cocosapp.core.BaseFragment;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.presentation.main.informacion.InformacionFragment;
import com.cerezaconsulting.cocosapp.presentation.main.promociones.PromoFragment;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 28/06/17.
 */

public class RestaurantFragment extends BaseFragment {
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.titles)
    CirclePageIndicator titles;
    @BindView(R.id.container_image)
    RelativeLayout containerImage;
    @BindView(R.id.text_container)
    LinearLayout textContainer;
    @BindView(R.id.layout_content_frame)
    FrameLayout layoutContentFrame;
    Unbinder unbinder;
    @BindView(R.id.btn_reservar)
    TextView btnReservar;
    @BindView(R.id.btn_uber)
    TextView btnUber;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public RestEntinty restEntinty;
    private InitAdapter initAdapter;
    //
    Bundle bundle;

    public RestaurantFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static RestaurantFragment newInstance(Bundle bundle) {
        RestaurantFragment fragment = new RestaurantFragment();

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restEntinty = (RestEntinty) getArguments().getSerializable("restEntity");

        bundle = new Bundle();
        bundle.putSerializable("restEntity", restEntinty);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_restaurante, container, false);
        FrameLayout frameLayout = rootView.findViewById(R.id.layout_content_frame);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View activityView = layoutInflater.inflate(R.layout.layout_tab, null, false);

        View view = layoutInflater.inflate(R.layout.layout_tab, null);

        frameLayout.addView(view);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.black));


        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAdapter = new InitAdapter(getActivity(), restEntinty);
        pager.setAdapter(initAdapter);
        titles.notifyDataSetChanged();
        titles.setViewPager(pager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        PromoFragment promoFragment = new PromoFragment();
        promoFragment.setArguments(bundle);

        InformacionFragment informacionFragment = new InformacionFragment();
        informacionFragment.setArguments(bundle);

        adapter.addFragment(promoFragment, "Promociones");
        adapter.addFragment(informacionFragment, "Información");
        viewPager.setAdapter(adapter);
    }

    @OnClick({R.id.btn_uber, R.id.btn_reservar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_uber:
                Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.ubercab"));
                startActivity(i);
                break;
            case R.id.btn_reservar: Bundle newBundle = new Bundle();
                newBundle.putString("tlf", restEntinty.getMobile());

                DialogReserva dialogReserva = new DialogReserva(getContext(), newBundle);
                dialogReserva.show();
                break;
        }
    }

    /*@OnClick(R.id.btn_reservar)
    public void onViewClicked() {

        Bundle newBundle = new Bundle();
        newBundle.putString("tlf", restEntinty.getMobile());

        DialogReserva dialogReserva = new DialogReserva(getContext(), newBundle);
        dialogReserva.show();


    }*/

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    public static class InitAdapter extends PagerAdapter {

        Context context;
        ArrayList<String> list;
        @BindView(R.id.im_slide)
        ImageView imSlide;
        private LayoutInflater layoutInflater;
        String item;
        RestEntinty rest;


        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public InitAdapter(Context context, RestEntinty restEntinty) {
            this.context = context;
            this.list = new ArrayList<>();
            this.layoutInflater = LayoutInflater.from(context);
            this.rest = restEntinty;
            list.add(rest.getPhoto1());
            list.add(rest.getPhoto2());
            list.add(rest.getPhoto3());

        }

        public Object instantiateItem(final ViewGroup collection, final int position) {
            item = list.get(position);
            final View view = layoutInflater.inflate(R.layout.item_slide, collection, false);
            ButterKnife.bind(this, view);
            Glide.with(context)
                    .load(item)
                    .into(imSlide);
            //imSlide.setImageDrawable(item);
            collection.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public void destroyItem(ViewGroup collection, int position,
                                Object view) {
            collection.removeView((View) view);
        }
    }


}
