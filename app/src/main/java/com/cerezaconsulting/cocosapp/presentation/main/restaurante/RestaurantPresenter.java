package com.cerezaconsulting.cocosapp.presentation.main.favoritos;

import android.content.Context;

import com.cerezaconsulting.cocosapp.data.entities.FavoritesResponse;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.data.entities.RestaurantResponse;
import com.cerezaconsulting.cocosapp.data.entities.trackholder.TrackEntityHolder;
import com.cerezaconsulting.cocosapp.data.local.SessionManager;
import com.cerezaconsulting.cocosapp.data.remote.ServiceFactory;
import com.cerezaconsulting.cocosapp.data.remote.request.ListRequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 28/06/17.
 */

public class FavoritosPresenter implements FavoritosContract.Presenter, RestItem {

    private FavoritosContract.View mView;
    private Context context;
    private SessionManager mSessionManager;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public FavoritosPresenter(FavoritosContract.View mView, Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "view cannot be null!");
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(this.context);
    }


    @Override
    public void start() {

    }


    @Override
    public void clickItem(RestaurantResponse restaurantResponse) {
        mView.clickItemRestaurante(restaurantResponse);
    }

    @Override
    public void deleteItem(RestaurantResponse restaurantResponse, int position) {

    }

    @Override
    public void loadOrdersFromPage(int page) {
        getRestaurante(page);

    }

    @Override
    public void loadfromNextPage() {

        if (currentPage > 0)
            getRestaurante(currentPage);

    }

    @Override
    public void startLoad() {
        if (!firstLoad) {
            firstLoad = true;
            loadOrdersFromPage(1);
        }
    }

    @Override
    public void getRestaurante(final int page) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<FavoritesResponse> orders = listRequest.getFavoritesRestaurants("Token "+mSessionManager.getUserToken() ,page);
        orders.enqueue(new Callback<FavoritesResponse>() {
            @Override
            public void onResponse(Call<FavoritesResponse> call, Response<FavoritesResponse> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.getListRestaurante(response.body().getFav());

                    if (response.body().getFav() != null) {
                        currentPage = page +1;
                    } else {
                        currentPage = -1;
                    }

                } else {
                    mView.showErrorMessage("Error al obtener las órdenes");
                }
            }

            @Override
            public void onFailure(Call<FavoritesResponse> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
