package com.cerezaconsulting.gespro.utils;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by junior on 08/12/16.
 */

public class UtilsFacebook {


    public static void onShareResult(Context context, Fragment view, String contentTitle,
                                     String contentDescription, String contentUrl, String imgUrl) {
        FacebookSdk.sdkInitialize(context);
        CallbackManager callbackManager = CallbackManager.Factory.create();
        final ShareDialog shareDialog = new ShareDialog(view);

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("SHARE", "success");
            }


            @Override
            public void onCancel() {
                Log.d("SHARE", "cancel");
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

       /* ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .build();

        shareDialog.show(content);*/


        if(contentUrl!=null){
            if (shareDialog.canShow(ShareLinkContent.class)) {

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(contentTitle)
                        .setContentDescription(contentDescription)
                        .setContentUrl(Uri.parse(contentUrl))
                        .build();

                shareDialog.show(linkContent);
            }
        } else{
            if (shareDialog.canShow(ShareLinkContent.class)) {

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(contentTitle)
                    .setContentDescription(contentDescription)
                    .build();

            shareDialog.show(linkContent);

            }
        }



    }

}
